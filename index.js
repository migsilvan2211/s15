console.log("Hello World");

let n1 = parseInt(prompt("Please enter your first number:"));
let n2 = parseInt(prompt("Please enter your second number:"));

let total = n1 + n2;

switch(true) {
	case total < 10:
		answer = n1 + n2;
		console.log("add two numbers");
		break;
	case total >= 10 && total <= 20:
		answer = n1 - n2;
		console.log("subtract two numbers");
		break;
	case total >= 21 && total <= 30:
		console.log("multiply two numbers");
		answer = n1 * n2;
		break;
	case total >= 31:
		console.log("divide two numbers");
		answer = n1 / n2;
}

if(answer >= 10) {
	alert("The total is: " + answer);
} 
else {
	console.warn("The total is: " + answer);
}


let age;
let name;

try {
	name = prompt("Please enter your name: ");
	if (name == null || name == "") {
		throw e;
	}	
}
catch {
	
	while (name == null || name == "") {
		name = prompt("retype name:");
	}
}


try {
	age = prompt("Please enter your age:");
	if(age == null || age =="") {
		throw e;
	}
}

catch {
	while(age == null || age == "" || isNaN(parseInt(age)) || parseInt(age) < 0) {
		age = prompt("Are you a time traveller? Please enter your proper age:");
	}
}

console.log("Your name is: " + name + " and your age is: " + age);

function isLegalAge(age) {
	if (age >= 18) {
		alert("You are of legal age.");
	}
	else {
		alert("You are not allowed here.");
	}
}

switch(true) {
	case (age >= 18 && age < 21):
		console.log("You are now allowed to party.");
		break;
	case (age >= 21 && age < 65):
		console.log("You are now part of the adult society.");
		break;
	case (age == 65):
		console.log("We thank you for your contribution to society.");
		break;
	default:
		console.log("Are you sure you're not an alien?");
}

try {
	throw new Error("test");
}

catch {
	console.warn("error received");
}

finally {
	isLegalAge(age);
}